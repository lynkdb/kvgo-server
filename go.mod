module github.com/lynkdb/kvgo-server

go 1.15

require (
	github.com/golang/snappy v0.0.4 // indirect
	github.com/hooto/hauth v0.0.0-20210108051852-c98e6c9355f9 // indirect
	github.com/hooto/hflag4g v0.0.0-20181023133020-1524604e73ef // indirect
	github.com/hooto/hlog4g v0.0.0-20210209123645-d070995e647f
	github.com/hooto/htoml4g v0.0.0-20210710081710-805c1a9ddf80
	github.com/hooto/httpsrv v0.0.0-20210820160451-a548e214d86d // indirect
	github.com/lessos/lessgo v0.0.0-20201010103753-2e2039a4eb3c // indirect
	github.com/lynkdb/kvgo v1.1.1
	github.com/lynkdb/kvspec v1.0.2 // indirect
	github.com/shirou/gopsutil/v3 v3.21.7 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/tklauser/go-sysconf v0.3.8 // indirect
	github.com/tklauser/numcpus v0.3.0 // indirect
	github.com/valuedig/apis v0.0.0-20210820155718-a6378369d199 // indirect
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
	golang.org/x/sys v0.0.0-20210820121016-41cdb8703e55 // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/genproto v0.0.0-20210820002220-43fce44e7af1 // indirect
)
